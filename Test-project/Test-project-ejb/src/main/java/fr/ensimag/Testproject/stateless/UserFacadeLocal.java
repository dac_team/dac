/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.stateless;

import fr.ensimag.Testproject.Annonce;
import fr.ensimag.Testproject.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author messaous
 */
@Local
public interface UserFacadeLocal {

    void create(User person);

    void edit(User person);

    void remove(User person);

    User find(Object id);
    
    boolean isEmailExistant(String email);
    
    List<User> findAll();

    List<User> findRange(int[] range);
    
    void addAchetees(List<Annonce> achetees, User user) throws Exception;

    int count();
    
}
