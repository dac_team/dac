/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.stateless;

import fr.ensimag.Testproject.Annonce;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author brenierr
 */
@Stateless
public class AnnonceFacade extends AbstractFacade<Annonce> implements AnnonceFacadeLocal {
    @PersistenceContext(unitName = "Test-project_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AnnonceFacade() {
        super(Annonce.class);
    }

    @Override
    public void annoncesPlusDispo(List<Annonce> annonces) {
        for(Annonce a : annonces) {
            a.setEstDispo(false);
            a.getUser().getProposees().remove(a);
            em.merge(a);
            em.merge(a.getUser());
        }
    }
    
}
