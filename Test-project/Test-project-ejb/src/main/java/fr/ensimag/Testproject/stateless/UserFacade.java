/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.stateless;

import fr.ensimag.Testproject.Annonce;
import fr.ensimag.Testproject.User;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author messaous
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserFacadeLocal {
    @PersistenceContext(unitName = "Test-project_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    @Override
    public boolean isEmailExistant(String email) {
        return (find(email) != null);
    }

    /**
     *
     * @param achetees
     * @param user
     * @throws Exception
     */
    @Override
    public void addAchetees(List<Annonce> achetees, User user) throws Exception {
        if ( user.getBzous() < achetees.size())
            throw new Exception("bzous insuffisants");
        else {
            user.setBzous(user.getBzous() - achetees.size());
            for(Annonce a : achetees) {
                user.add_achetees(a);

            }
            // TODO 
            //
            em.merge(user);
            System.out.println("J'ai acheté : "+user.getAchetees().size()+ "annonces");
        }
    }
   
}
