/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author messaous
 */
@Entity
public class Competence implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String name;

    public String getName() {
        return name;
    }
    
    public Competence() {
        
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Competence(String name) {
        this.name = name;
    }


    
}
