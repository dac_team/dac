/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject;

import fr.ensimag.Testproject.Roles.Rolename;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author messaous
 */
@Entity(name="BZOUSUSER")
@ManagedBean
public class User implements Serializable  {
    private static final long serialVersionUID = 1L;
    
    @NotNull( message = "Veuillez saisir un prénom" )
    private String firstname;
    @Column(name="PASSWORD")
    private char[] password;
    private String name;
    @Id
    @Column(name="EMAIL")
    private String mail;
    private Integer gender;
    private Integer zipcode; 
    private String birthdate;
    
    private Integer bzous = 2;
    
    @OneToMany
    private List<Competence> competences;
    
    @OneToMany
    private List<Annonce> a_realiser;
    
    @OneToMany
    private List<Annonce> achetees;
    @OneToMany
    private List<Annonce> proposees;
    @OneToMany
    private List<Disponibilites> disponibilites;

    public List<Annonce> getProposees() {
        return proposees;
    }

    public void setProposees(List<Annonce> proposees) {
        this.proposees = proposees;
    }

    
    public Integer getBzous() {
        return bzous;
    }

    public void setBzous(Integer bzous) {
        this.bzous = bzous;
    }

    public List<Competence> getCompetences() {
        return competences;
    }

    public void setCompetences(List<Competence> competences) {
        this.competences = competences;
    }

    public List<Annonce> getA_realiser() {
        return a_realiser;
    }

    public void setA_realiser(List<Annonce> a_realiser) {
        this.a_realiser = a_realiser;
    }

    public List<Annonce> getAchetees() {
        return achetees;
    }

    public void setAchetees(List<Annonce> achetees) {
        this.achetees = achetees;
    }

    public List<Disponibilites> getDisponibilites() {
        return disponibilites;
    }

    public void setDisponibilites(List<Disponibilites> disponibilites) {
        this.disponibilites = disponibilites;
    }
    
    
    
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL, mappedBy="user")
    private Roles role;
    

    public User() {
        
    }
    
    public User(String firstname, String name, char[] password, String mail, Integer gender, Integer zipcode, String birthdate) {
        this.firstname = firstname;
        this.name = name;
        this.password = hashPassword(password);
        this.mail = mail;
        this.gender = gender;
        this.zipcode = zipcode;
        this.role = new Roles(Rolename.USER,this);
        this.birthdate = birthdate;
        this.a_realiser = new ArrayList<>();
        this.achetees = new ArrayList<>();
        this.proposees = new ArrayList<>();
        this.disponibilites = new ArrayList<>();
    }
    
    public void addCompetence(Competence c) {
        this.competences.add(c);
    }
    
    public void add_aRealiser(Annonce a) {
        this.a_realiser.add(a);
    }
    
    public void add_achetees(Annonce a) throws Exception {
        if (a.getUser()==this) {
            throw new Exception("Tu ne peux pas souscrire à un cours que tu donnes");
        }
    
        this.achetees.add(a);
        
        
    }
    
    public void add_proposees(Annonce a) {
        this.proposees.add(a);
    }
    
    public static char[] hashPassword(char[] password) {
        char[] encoded = null;
        try {
            ByteBuffer passwdBuffer = 
              Charset.defaultCharset().encode(CharBuffer.wrap(password));
            byte[] passwdBytes = passwdBuffer.array();
            MessageDigest mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(passwdBytes, 0, password.length);
            encoded = new BigInteger(1, mdEnc.digest()).toString(16).toCharArray();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }

        return encoded;
    }
        
    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
    public char[] getPassword() {
        return password;
    }
    
    public void setRole(Roles role) {
        this.role = role;
        role.setUser(this);
    }

    public Roles getRole() {
        return role;
    }
    public void setPassword(char[] password) {
        this.password = hashPassword(password);
    }
    
    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipCode) {
        this.zipcode = zipCode;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


}
