/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author messaous
 */
@Entity(name="ROLES")
public class Roles implements Serializable {
    private static final long serialVersionUID = 1L;
   // private String rolename;

    
    @Id
    @Enumerated(EnumType.STRING)
    @Column(name="ROLE_NAME")
    private Rolename rolename;
    
    @Id
    @OneToOne
    @JoinColumn(name = "USER_NAME")
    private User user;

    void setUser(User user) {
      this.user = user;
    }
    public static enum Rolename {USER, ADMIN};
    public Roles() {
        
    }
    
    public Roles(Rolename rolename, User user) {
        this.rolename = rolename;
        this.user = user;
    }
    /**
     * Get the value of role
     *
     * @return the value of role
     */
    public Rolename getRole() {
        return rolename;
    }

    /**
     * Set the value of role
     *
     * @param role new value of role
     */
    public void setRole(Rolename role) {
        this.rolename = role;
    }

  /*  public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Roles)) {
            return false;
        }
        Roles other = (Roles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.ensimag.Testproject.Roles[ id=" + id + " ]";
    }
    */
}
