/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.stateful;

import fr.ensimag.Testproject.Annonce;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author messaous
 */
@Stateful
public class PanierFacade implements PanierFacadeLocal {
    @PersistenceContext(unitName = "Test-project_PU")
    private EntityManager em;  
    
    private List<Annonce> annonces;


    public PanierFacade() {
        annonces = new ArrayList<Annonce>();
    }
    
    
    @Override
    public void addAnnonce(Annonce a) {
        annonces.add(a);
    }

    @Override
    public void removeAnnonce(Annonce a) {
        annonces.remove(a);
    }

    @Override
    public List<Annonce> getAnnonces() {
        return annonces;
    }


    @Override
    public void videPanier() {
        annonces.clear();
    }
    
}
