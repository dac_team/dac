/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.controler.person;

import fr.ensimag.Testproject.Annonce;
import fr.ensimag.Testproject.Competence;
import fr.ensimag.Testproject.User;
import fr.ensimag.Testproject.stateful.PanierFacade;
import fr.ensimag.Testproject.stateful.PanierFacadeLocal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author brenierr
 */
@ManagedBean
@SessionScoped // Ce controller est valable pour toute la session courante
public class PanierBean implements java.io.Serializable {
        
    /** Identifiant du dernier numéro d'article ajouter au panier dans la session */
    private Integer numeroLastArticle = 0;
    
    private static final String PANIER_BEAN_SESSION_KEY = "Panier";
    
    public Integer getNumeroLastArticle() {
        return numeroLastArticle;
    }

    /**
     * Fonction qui recupere une session à partir du faces context 
     */
    private PanierFacadeLocal getSession(){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        PanierFacadeLocal panier = (PanierFacadeLocal) request.getSession().getAttribute(PANIER_BEAN_SESSION_KEY);
        if ( panier == null) {
        //    try {
                //InitialContext ic = new InitialContext();
                
                //lookup                
                panier = new PanierFacade();  
                System.out.println("test test ");
                request.getSession().setAttribute(PANIER_BEAN_SESSION_KEY, panier);
                
        /*    } catch ( NamingException e) {
                throw new ServletException(e);
            }*/
        }
        return panier;
    }


    
    /** 
     * Fonction qui permet de recuperer l'age dans la session 
     * @return l'age 
     */
    public List<Annonce> getArticles(){
        PanierFacadeLocal panier = getSession();    
        return panier.getAnnonces();
    }
    
    // TODO : a enlever c'est que pour les tests 
     
    @EJB
    private fr.ensimag.Testproject.stateless.UserFacadeLocal userFacade;
    @EJB
    private fr.ensimag.Testproject.stateless.CompetenceFacadeLocal competenceFacade;
    @EJB
    private fr.ensimag.Testproject.stateless.AnnonceFacadeLocal annonceFacade;

    public void addArticle() {
        System.out.println("test model 1" + annonceFacade.findAll().size());
        if (numeroLastArticle==0) {
            char[] password = "azerty".toCharArray();
            User p = new User("duzaeapond", "jean", password,"brenier5@", 0, 27932, "01/01/2012");
            userFacade.create(p);


            PanierFacadeLocal panier = getSession();
            Competence c = new Competence("maths");
            competenceFacade.create(c);
            Date date = new Date();
            Integer heure = 1;       
            Annonce annonce = new Annonce(c,p,date,heure);
            annonceFacade.create(annonce);       
            panier.addAnnonce(annonce);


            c= new Competence("fr");
            competenceFacade.create(c);
            annonce = new Annonce(c,p,date,heure);
            annonceFacade.create(annonce);

            System.out.println("test model " + annonceFacade.findAll().size());
            panier.addAnnonce(annonce);
        } else {
            char[] password = "azerty".toCharArray();
            User p = new User("duzaeapond", "jean", password,"brenier6@", 0, 27932, "01/01/2012");
            userFacade.create(p);


            PanierFacadeLocal panier = getSession();
            Competence c = new Competence("maths2");
            competenceFacade.create(c);
            Date date = new Date();
            Integer heure = 1;       
            Annonce annonce = new Annonce(c,p,date,heure);
            annonceFacade.create(annonce);       
            panier.addAnnonce(annonce);


            c= new Competence("fr2");
            competenceFacade.create(c);
            annonce = new Annonce(c,p,date,heure);
            annonceFacade.create(annonce);

            System.out.println("test model " + annonceFacade.findAll().size());
            panier.addAnnonce(annonce);
        }
        numeroLastArticle++;
        
    }
    
    public Integer getArticlesSize() {
        System.out.println("size dans getArticles" + getArticles().size());
        return getArticles().size();
    }
}
