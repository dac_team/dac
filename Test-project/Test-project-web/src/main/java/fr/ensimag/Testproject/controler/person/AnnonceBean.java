
package fr.ensimag.Testproject.controler.person;

import fr.ensimag.Testproject.Annonce;
import fr.ensimag.Testproject.User;
import fr.ensimag.Testproject.stateless.AnnonceFacadeLocal;
import fr.ensimag.Testproject.stateless.UserFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
/**
*
* @author reignier
*/
@Named(value = "annonceBean")
@RequestScoped
public class AnnonceBean {
    @EJB
    private AnnonceFacadeLocal annonceFacade;
    @EJB
    private UserFacadeLocal userFacade;
        
    private List<AnnonceSelected> annonces;
    private User user;

    public User getUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        String email = request.getRemoteUser();
        user = userFacade.find(email);
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

  
    @PostConstruct
    public void init() {
        annonces = new ArrayList<>();
        for(Annonce a : annonceFacade.findAll()) {
            if (a.getEstDispo()) {
                AnnonceSelected ann = new AnnonceSelected(a, false);
                annonces.add(ann);
            }
        }
    }
    
    public List<AnnonceSelected> getAnnonces() {
        return annonces;
    }

/*
            <p:outputLabel value="Annonces disponibles" />
        <p:selectManyMenu value="#{annonceBean.selected}" showCheckbox="true" converter="annonceConverter" filter="true" filterMatchMode="contains">
            <f:selectItems value="#{annonceBean.annonces}" var="annonce" itemLabel="#{annonce.competence.name}" />

        </p:selectManyMenu>
    */
    public String buyAnnonces(){
        user = getUser();
        if ( user == null)
            System.out.println("erreur achat, utilisateur non connecté");
        else {
            List<Annonce> toBuy = new ArrayList<>();
            for(AnnonceSelected a : annonces) {
                if (a.getSelected()) {
                    toBuy.add(a.getAnnonce());
                }
            }
            try {
               userFacade.addAchetees(toBuy, user);
                // Success : enlever les annonces des annonces dispos
                annonceFacade.annoncesPlusDispo(toBuy);
            } catch (Exception ex) {
                Logger.getLogger(AnnonceBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        init();
        return "/secure/achatAnnonce.xhtml?faces-redirect=true";
    }

}