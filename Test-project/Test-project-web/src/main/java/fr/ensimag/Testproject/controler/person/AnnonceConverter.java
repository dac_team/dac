/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.controler.person;

import fr.ensimag.Testproject.Annonce;
import fr.ensimag.Testproject.stateless.AnnonceFacadeLocal;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author messaous
 */
@FacesConverter("annonceConverter")
public class AnnonceConverter implements Converter {
    
    private List<Annonce> annonces;
    
    @EJB
    private AnnonceFacadeLocal annonceFacade;
    
  
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println("CONVERTER ");
        if(value != null && value.trim().length() > 0) {
                return annonceFacade.find(Integer.parseInt(value));
            }
        else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            return String.valueOf(((Annonce) object).getId());
        }
        else {
            return null;
        }
    }   

    public Annonce getMyObject(String id) {
        Iterator<Annonce > iterator = this.annonces.iterator();
        while(iterator.hasNext()) {
            Annonce object = iterator.next();

            if(object.getId() == Integer.valueOf(id).intValue()) {
                return object;
            }
        }
        return null;
    }


}

