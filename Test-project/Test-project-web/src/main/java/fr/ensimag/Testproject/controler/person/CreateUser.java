/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.controler.person;

import fr.ensimag.Testproject.User;
import fr.ensimag.Testproject.stateless.UserFacadeLocal;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
/**
*
* @author reignier
*/
@Named(value = "registerUser")
@RequestScoped
public class CreateUser {
    @EJB
    private UserFacadeLocal userFacade;
    private User user = null;
    private String password;
    private String passwordConfirm;
    /**
    * Creates a new instance of Create
    */
    public CreateUser() {
    }
    
    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public String getPassword() {
        return password;
    }
    
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public void setPasswordConfirm( String passwordConfirm){
        this.passwordConfirm = passwordConfirm;
    }
    
    public boolean ConfirmPassword(FacesContext context){    
       if(!((String)password).equals(passwordConfirm)){   
           System.out.println("Mots de passe non identiques " + password + " " + password.getClass().getName() + " " + passwordConfirm + " " +passwordConfirm.getClass().getName());
           context.addMessage("registerForm:gridpasswordConfirm:passwordConfirm", new FacesMessage("Erreur", "Mots de passe non identiques! Réessayez"));        
            return false;
       }
       return true;
    }
    
    public boolean checkPassword(FacesContext context) {
        if (password.length()<8) {
            System.out.println("PAS ASSEZ CAR");
            context.addMessage("registerForm:gridpassword:password", new FacesMessage("Erreur", "Mot de passe, minimum 8 caractères"));
            return false;
        }
        return true;
    }
    
    public String save()
    {
        char[] pswd = password.toCharArray();
        User newUser = new User(user.getFirstname(), user.getName(), pswd, user.getMail(), user.getGender(), user.getZipcode(), user.getBirthdate());
        FacesContext context = FacesContext.getCurrentInstance();
        boolean passwordOK = checkPassword(context);
        boolean passwordConfirmOK = ConfirmPassword(context);
        if (!passwordOK) {
            return "registerUser.xhtml?faces-redirect=false";
        }
        if (passwordOK && !passwordConfirmOK) {
            return "registerUser.xhtml?faces-redirect=false";
        }
        try {
            userFacade.create(newUser);
        } catch (javax.ejb.EJBException e) {
            context.addMessage("message", new FacesMessage("Erreur", "Email incorrect"));
            return "registerUser.xhtml?faces-redirect=true";
        }
        context.addMessage("message", new FacesMessage("Info", "Enregistrement réussi"));
        return "SignInUser.xhtml?faces-redirect=true";
    }
    
    public String retourAccueil(){
        return "index.xhtml?faces-redirect=true";
    }
    
    public String goToListeCours(){
        return "ListeCours.xhtml?faces-redirect=true";
    }
}