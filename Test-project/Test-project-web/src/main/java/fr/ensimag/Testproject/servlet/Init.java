/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.servlet;

import fr.ensimag.Testproject.Annonce;
import fr.ensimag.Testproject.Competence;
import fr.ensimag.Testproject.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author messaous
 */
@WebServlet(name = "Init", urlPatterns = {"/Init"})
public class Init extends HttpServlet {
    
    @EJB
    private fr.ensimag.Testproject.stateless.UserFacadeLocal userFacade;
    @EJB
    private fr.ensimag.Testproject.stateless.CompetenceFacadeLocal competenceFacade;
    @EJB
    private fr.ensimag.Testproject.stateless.AnnonceFacadeLocal annonceFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Init</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Init at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        char[] password = "azerty".toCharArray();
        User p1 = new User("dupond", "jean", password, "brenier@", 0, 38000, "01/01/2012");
        userFacade.create(p1);
        User p2 = new User("eajiz", "jean", password,"brenier2@", 0, 27932, "01/01/2012");
        userFacade.create(p2);
        User p3 = new User("duzaeapond", "jean", password,"brenier3@", 0, 27932, "01/01/2012");
        userFacade.create(p3);
        User p4 = new User("kelly", "john", password,"brenier4@", 0, 59000, "01/01/2012");      
        userFacade.create(p4);
        

        Competence c = new Competence("maths");
        competenceFacade.create(c);
        Date date = new Date();
        Integer heure = 1;
        
        Annonce annonce = new Annonce(c,p3,date,heure);
        annonceFacade.create(annonce);
        
        c= new Competence("fr");
        competenceFacade.create(c);

        annonce = new Annonce(c,p2,date,heure);
        annonceFacade.create(annonce);
        
        annonce = new Annonce(c,p4,date,heure);
        annonceFacade.create(annonce);

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
