/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.controler.person;

import fr.ensimag.Testproject.Annonce;

/**
 *
 * @author messaous
 */
public class AnnonceSelected {
    private Annonce annonce;
    
    public AnnonceSelected(Annonce annonce, boolean selected) {
        this.annonce = annonce;
        this.selected = selected;
    }

    public Annonce getAnnonce() {
        return annonce;
    }

    public void setAnnonce(Annonce annonce) {
        this.annonce = annonce;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    private boolean selected;
}
