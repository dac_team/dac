/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.controler.person;


import fr.ensimag.Testproject.Annonce;
import fr.ensimag.Testproject.Competence;
import fr.ensimag.Testproject.User;
import fr.ensimag.Testproject.stateless.AnnonceFacadeLocal;
import fr.ensimag.Testproject.stateless.CompetenceFacadeLocal;
import fr.ensimag.Testproject.stateless.UserFacadeLocal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author tonnoira
 */
@ManagedBean
@SessionScoped
@Named(value = "competenceBean")
public class CompetenceBean {
    @EJB
    private CompetenceFacadeLocal competenceFacade;
    @EJB
    private AnnonceFacadeLocal annonceFacade;
    
    public List<Competence> competences;
    public String selectedCompetence;
    public boolean affiche;
    String Lieu;
    public List<Annonce> toPrint;
    
    @PostConstruct
    public void init(){
        System.out.println("bananaaaaa");
        competences = new ArrayList<>();
        toPrint = new ArrayList<>();
        for(Competence a : competenceFacade.findAll()) {
                competences.add(a);
        }        
        competences.add(new Competence("dev"));
        competences.add(new Competence("trade"));
        affiche = false;
    }
    
    public void update(){
        for(Competence a : competenceFacade.findAll()) {
            if(!competences.contains(a)){
                competences.add(a);
            }
        }
    }
         
    public List<Competence> getCompetences(){
        return competences;
    }
    
    public void setCompetences(List<Competence> comp){
        this.competences = comp;
    }
    
    public String getSelectedCompetence(){
        return selectedCompetence;
    }
    
    public void setSelectedCompetence(String s){
        this.selectedCompetence = s;
    }
    
    public boolean isShow(){
        return affiche;
    }
    
    public List<Annonce> getToPrint(){
        return toPrint;
    }
    
    public String recherche(){
        update();
        HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String lieu = request.getParameter("lieu");
        this.Lieu = lieu;
        affiche = true;
        
        System.out.println("lieu " + lieu);
        toPrint.clear();
        for(Annonce a : annonceFacade.findAll()) {
            //System.out.println(a.getUser().getZipcode());
            //System.out.println(a.getCompetence().getName());
            if(lieu.equals("") || lieu.equals("Zipcode")){
                //System.out.println("lieu 0");
                if(a.getCompetence().getName().equals(selectedCompetence)){
                    //System.out.println("bonne comp");
                    toPrint.add(a);
                }                              
            }else{
                //System.out.println("lieu");
                //System.out.println(Integer.parseInt(lieu) + " " + a.getUser().getZipcode());
                if(a.getUser().getZipcode() == Integer.parseInt(lieu)){
                    //System.out.println("bon lieu");
                  if(a.getCompetence().getName().equals(selectedCompetence)){
                      //System.out.println("bonne comp");
                        toPrint.add(a);
                    }    
                }
            }
        }
        return "index.xhtml?faces-redirect=true";
    }
    
}
