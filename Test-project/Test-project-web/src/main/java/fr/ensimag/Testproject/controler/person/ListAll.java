/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.Testproject.controler.person;

import fr.ensimag.Testproject.User;
import fr.ensimag.Testproject.stateless.UserFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author messaous
 */
@Named(value = "listAllUsers")
@RequestScoped
public class ListAll {

    private List<User> users = null;
    @EJB
    private UserFacadeLocal userFacade;
    
    public List<User> getUsers() {
        if (users == null)
            users = userFacade.findAll();
        return users;
    }
    /**
     * Creates a new instance of ListAll
     */
    public ListAll() {
    }
    
}
