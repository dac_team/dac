1) Telecharger glassfish server décrit dans le tutoriel du cours (partie 3) Serveur d’application)
2) Télécharger postgre sql et le mettre dans glassfish4/glassfish/domains/domain1/lib
3) Ouvrir l'adresse http://localhost:4848 sur votre navigateur
4) Aller dans l'onglet Configurations (à gauche) -> server-config -> Security -> Realms 
5) Une fois que vous avez cliqué sur le dossier Realms, Cliqué sur New 
6) Dans le champ Name: rentrer jdbcRealm 
7) Dans le champ Class Name: sélectionner JDBCRealm

Remplir tous les champs comme dans l'image ajouter au git glassfish_config.jpg

8) Telecharger le git (bitbucket)
9) Ouvrir netbeans 
10) Faire ouvrir projet existant et selectionner le projet Test-project et les trois en dessous 
11) Aller dans l'onglet Services. Selectionner Servers et faire clique droit -> add server
12) Selectionner la racine du dossier glassfish crée plus tôt pour le champ Installation Location
13) Une fois le server crée, faire clique droit dessus -> Start 
14) Se positionner sur Test-project (projet global) et lancer un build
15) Se positionner sur Test-project-ear et faire play (flèche verte)
16) Attendez, une page web doit s'ouvrir et le projet est lancé sur votre machine 


